Test technique
==============

Objectif
--------

Ce test technique est composé de 4 exercices d'algorithmie.  

Pour chaque exercice, l'objectif est de compléter la classe ``src/ExerciceN.php``,
en implémentant la méthode ``run()`` pour répondre à l'énoncé de l'exercice :

```
<?php

final class Exercice1
{
    public static function run(int $number): string
    {
        // TODO : méthode run() à implémenter pour résoudre l'exercice n°1
        return '';
    }
}
```

Il faut également écrire les tests unitaires associés en complétant la méthode ``testRun()`` de la classe ``tests/ExerciceNTest.php`` :

```
<?php

use PHPUnit\Framework\TestCase;

final class Exercice1Test extends TestCase
{
    public function testRun(): void
    {
        // TODO : tests correspondant à la méthode Exercice1::run() à écrire

        // Exemple de test
        $this->assertSame('2', Exercice1::run(2));
    }
}
```

Lancement des tests unitaires
-----------------------------

Si composer est installé sur l'environnement local, se positionner dans le répertoire projet et lancer les commandes suivantes :

```
$ composer install
$ ./vendor/bin/phpunit tests
```

Sinon, il est possible d'utiliser Docker :

```
$ docker run --rm --interactive --tty --volume $PWD:/app composer install
$ docker run --rm --interactive --tty --volume $PWD:/app --entrypoint ./vendor/bin/phpunit composer tests
```

Déroulé du test technique
-------------------------

Une heure de début du test technique sera convenue avec le candidat.  
L'énoncé des 4 exercices sera alors transmis au candidat à cette heure précise.  
Une fois les exercices terminés, le candidat retournera les exercices complétés à l'adresse suivante :   

**pierre.lecavelier@cropandco.com**

La durée du test technique est estimée entre 30 minutes et 1 heure.