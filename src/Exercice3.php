<?php

final class Exercice3
{
    /**
     * @param string[] $notes liste des 3 notes (séparées par des espaces) pour chaque restaurant
     * @return int|null la note la plus élevée arrondie à l'entier supérieur (null s'il n'y a aucune note en entrée)
     */
    public static function run(array $notes): ?int
    {
        // TODO : méthode run() à implémenter pour résoudre l'exercice n°3
        return null;
    }
}