<?php

use PHPUnit\Framework\TestCase;

final class Exercice1Test extends TestCase
{
    public function testRun(): void
    {
        // TODO : tests correspondant à la méthode Exercice1::run() à écrire

        // Exemple de test
        $this->assertSame('2', Exercice1::run(2));
    }
}