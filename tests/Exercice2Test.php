<?php

use PHPUnit\Framework\TestCase;

final class Exercice2Test extends TestCase
{
    public function testRun(): void
    {
        // TODO : tests correspondant à la méthode Exercice2::run() à écrire

        // Exemple de test
        $this->assertSame(0, Exercice2::run(1, 1, 1, 1));
    }
}