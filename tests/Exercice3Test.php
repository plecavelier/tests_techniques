<?php

use PHPUnit\Framework\TestCase;

final class Exercice3Test extends TestCase
{
    public function testRun(): void
    {
        // TODO : tests correspondant à la méthode Exercice3::run() à écrire

        // Exemple de test
        $this->assertSame(10, Exercice3::run([ '5 10 15', '10 10 10', '15 10 5' ]));
    }
}