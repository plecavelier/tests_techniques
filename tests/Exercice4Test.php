<?php

use PHPUnit\Framework\TestCase;

final class Exercice4Test extends TestCase
{
    public function testRun(): void
    {
        // TODO : tests correspondant à la méthode Exercice4::run() à écrire

        // Exemples de tests
        $this->assertSame(1, Exercice4::run('----------------'));
        $this->assertSame(3, Exercice4::run('---__------_----'));
    }
}